#A program to guess birthdays and stuff
from random import randint

name = input("Hi, please enter your name: ")
correct = False
guesses = 0
months = ("January","February","March","April","May","June","July","August","September","October","November","December")
print("Thank you for telling me your name "+name+"!")
while correct == False and guesses<=4:
    monthguessint = randint(1,12)
    monthguessstr = months[monthguessint - 1]
    yearguessint = randint(1924,2004)
    yearguessstr = str(yearguessint)
    response = input(name+", were you born in "+monthguessstr+" of "+yearguessstr+"? y/n")
    if response == "y":
        print("I Knew it!")
        correct = True
    else:
        print("Drat! Lemme Try again!")
        guesses = guesses + 1

if correct == True:
    exit()
else:
    print("I have other things to do. Good bye.")
    exit()
